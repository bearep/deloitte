# -*- coding: utf-8 -*-
# Author: bear

import unittest

from flask import url_for

from app import app, db
from app.errors.project_errors import BadRequestException, AccDoesntActivatedException, LoginException, \
    NotFoundException, NotEnoughRightsException, DataValidationException
from app.models.models import User, Note
from app.resolvers.user_resolvers import Auth
from app.resolvers.note_resolvers import Notes


class AuthCase(unittest.TestCase):
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.config['MAIL_SERVER'] = 'localhost'
        app.config['MAIL_PORT'] = '8025'
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_create_user(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")

        self.assertTrue(u1 is not None)
        self.assertEqual(u1.get('username'), "test_user")
        self.assertEqual(u1.get('email'), "a@a.com")
        self.assertEqual(u1.get('activated'), False)
        self.assertNotEqual(u1.get('activated'), True)

        self.assertRaises(BadRequestException, Auth.create_user, "", "a1@a.com", "password")
        self.assertRaises(BadRequestException, Auth.create_user, "test_user4", "a1a.com", "password")
        self.assertRaises(BadRequestException, Auth.create_user, "test_user", "a1@a.com", "password")
        self.assertRaises(BadRequestException, Auth.create_user, "test_user3", "a@a.com", "password")

        u4 = Auth.create_user("test_user4", "a4@a.com", "password")
        self.assertTrue(u4 is not None)
        self.assertEqual(u4.get('username'), "test_user4")
        self.assertEqual(u4.get('email'), "a4@a.com")
        self.assertEqual(u4.get('activated'), False)
        self.assertNotEqual(u4.get('activated'), True)

    def test_get_activation_token(self):
        user_id = 123
        token = Auth.get_activation_token(user_id)
        user_id1 = Auth.get_user_id_by_token(f"{token}1")
        user_id2 = Auth.get_user_id_by_token(token)

        self.assertEqual(user_id, user_id2)
        self.assertNotEqual(user_id, user_id1)

    def test_activate_user(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        u_db = User.query.get(u1['id'])
        self.assertEqual(u_db.activated, False)

        token = Auth.get_activation_token(u1['id'])
        Auth.activate_user(token)

        u_db = User.query.get(u1['id'])
        self.assertNotEqual(u_db.activated, False)
        self.assertEqual(u_db.activated, True)

    def test_get_user_token(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        user_id = u1.get('id')
        self.assertRaises(LoginException, Auth.get_user_token, "dddd", "password")
        self.assertRaises(LoginException, Auth.get_user_token, "test_user", "password1")
        self.assertRaises(AccDoesntActivatedException, Auth.get_user_token, "test_user", "password")

        activate_token = Auth.get_activation_token(user_id)
        Auth.activate_user(activate_token)

        with app.app_context():
            login_token = Auth.get_user_token("test_user", "password")
            self.assertIsInstance(login_token, str)


class NotesCase(unittest.TestCase):
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.config['SERVER_NAME'] = 'localhost'
        self.ctx = app.app_context()
        self.ctx.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.ctx.pop()

    def test_create_note(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        user_id = u1.get('id')
        notes_count = Note.query.filter_by(user_id=user_id).count()
        self.assertEqual(notes_count, 0)

        n1 = Notes.create_note(user_id, dict(title="some_title", text="some_text"))
        n2 = Notes.create_note(user_id, dict(title="some_title2", text="some_text2"))

        notes_count = Note.query.filter_by(user_id=user_id).count()
        self.assertEqual(notes_count, 2)

        self.assertTrue(n1 is not None)
        self.assertEqual(n1.get('title'), "some_title")
        self.assertEqual(n1.get('text'), "some_text")

        self.assertTrue(n2 is not None)
        self.assertEqual(n2.get('title'), "some_title2")
        self.assertEqual(n2.get('text'), "some_text2")

        n1_db = Note.query.get(n1.get('id'))

        self.assertNotEqual(n1_db, None)
        self.assertEqual(n1.get('title'), n1_db.title)
        self.assertEqual(n1.get('id'), n1_db.id)
        self.assertEqual(n1.get('text'), n1_db.text)

        # check field validation
        n3 = dict(title_="some_title", text="some_text")
        self.assertRaises(DataValidationException, Notes.create_note, user_id, n3)

    def test_get_note(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        u2 = Auth.create_user("test_user2", "a2@a.com", "password")
        user1_id = u1.get('id')
        user2_id = u2.get('id')

        n1 = Notes.create_note(user1_id, dict(title="some_title", text="some_text"))

        self.assertRaises(NotFoundException, Notes.get_note, 666, user1_id)
        self.assertRaises(NotEnoughRightsException, Notes.get_note, n1.get('id'), user2_id)

        n1_get = Notes.get_note(n1.get('id'), user1_id)

        self.assertTrue(n1_get is not None)
        self.assertEqual(n1_get.get('title'), "some_title")
        self.assertEqual(n1_get.get('text'), "some_text")

    def test_delete_note(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        u2 = Auth.create_user("test_user2", "a2@a.com", "password")
        user1_id = u1.get('id')
        user2_id = u2.get('id')

        n1 = Notes.create_note(user1_id, dict(title="some_title", text="some_text"))
        notes_count = Note.query.filter_by(user_id=user1_id).count()
        self.assertEqual(notes_count, 1)

        self.assertRaises(NotFoundException, Notes.delete_note, 666, user1_id)
        self.assertRaises(NotEnoughRightsException, Notes.delete_note, n1.get('id'), user2_id)

        Notes.delete_note(n1.get('id'), user1_id)
        notes_count = Note.query.filter_by(user_id=user1_id).count()
        self.assertEqual(notes_count, 0)

    def test_edit_note(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        u2 = Auth.create_user("test_user2", "a2@a.com", "password")
        user1_id = u1.get('id')
        user2_id = u2.get('id')

        n1 = Notes.create_note(user1_id, dict(title="some_title", text="some_text"))
        note_data = dict(title="new_title", text="new_text")
        self.assertRaises(NotFoundException, Notes.edit_note, 666, user1_id, note_data)
        self.assertRaises(NotEnoughRightsException, Notes.edit_note, n1.get('id'), user2_id, note_data)

        # check field validation
        wrong_data = dict(title_="some_title", text="some_text")
        self.assertRaises(DataValidationException, Notes.edit_note, n1.get('id'), user1_id, wrong_data)

        n1_edit = Notes.edit_note(n1.get('id'), user1_id, note_data)

        self.assertTrue(n1_edit is not None)
        self.assertEqual(n1_edit.get('title'), "new_title")
        self.assertEqual(n1_edit.get('text'), "new_text")

        n1_db = Note.query.get(n1_edit.get('id'))

        self.assertTrue(n1_db is not None)
        self.assertEqual(n1_db.title, "new_title")
        self.assertEqual(n1_db.text, "new_text")

        self.assertEqual(n1_db.id, n1_edit.get('id'))
        self.assertEqual(n1_db.id, n1.get('id'))

    def test_get_notes(self):
        u1 = Auth.create_user("test_user", "a@a.com", "password")
        u2 = Auth.create_user("test_user2", "a2@a.com", "password")
        user1_id = u1.get('id')
        user2_id = u2.get('id')

        u1_notes = Notes.get_notes(user1_id)
        self.assertListEqual(u1_notes, [])
        n1_data = dict(title="some_title", text="some_text")
        n2_data = dict(title="some_title2", text="some_text2")
        n1 = Notes.create_note(user1_id, n1_data.copy())
        n2 = Notes.create_note(user1_id, n2_data.copy())
        u1_notes = Notes.get_notes(user1_id)
        n1_data['id'] = n1['id']
        n1_data['_links'] = {'self': url_for('get_note', note_id=n1['id']), "collection": url_for("get_notes")}

        n2_data['id'] = n2['id']
        n2_data['_links'] = {'self': url_for('get_note', note_id=n2['id']), "collection": url_for("get_notes")}
        self.assertListEqual(sorted(u1_notes, key=lambda x: x['id']), sorted([n2_data, n1_data], key=lambda x: x['id']))

        u2_notes = Notes.get_notes(user2_id)
        self.assertListEqual(u2_notes, [])
