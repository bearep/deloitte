FROM python:3.7-alpine

RUN adduser -D deloitte

WORKDIR /home/deloitte
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
COPY requirements.txt requirements.txt 
RUN pip install -r requirements.txt
COPY app app
COPY migrations migrations
COPY deloitte.py config.py web_boot.sh ./
RUN chmod +x web_boot.sh
RUN chown -R deloitte:deloitte ./

EXPOSE 5000
ENV FLASK_APP=deloitte.py
USER deloitte
ENTRYPOINT ["./web_boot.sh"]