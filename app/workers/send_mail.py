# -*- coding: utf-8 -*-
# Author: bear

from flask_mail import Mail, Message
from celery import Celery
from kombu import Queue

from app import app

mail = Mail(app)
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])

celery.conf.task_queues = (Queue('default', routing_key='main'), Queue('mail_queue', routing_key="email"))
celery.conf.task_default_queue = "default"

celery.conf.task_routes = {'app.workers.send_mail.send_async_email': {"queue": "mail_queue"}}


@celery.task(default_retry_delay=300, max_retries=5)
def send_async_email(email_data):
    msg = Message(email_data['subject'], sender=app.config['MAIL_DEFAULT_SENDER'], recipients=[email_data['to']])
    msg.body = email_data['body']
    with app.app_context():
        mail.send(msg)
