# -*- coding: utf-8 -*-
# Author: bear
from flask import jsonify, request

from app import app
from app.controllers.base import check_request_data
from app.resolvers.user_resolvers import Auth


@app.route('/users', methods=['POST'])
@check_request_data(keys=('username', 'email', 'password'))
def create_user():
    data = request.get_json()

    user_dto = Auth.create_user(data['username'], data['email'], data['password'])
    Auth.send_activation_email(user_dto['id'], user_dto['email'])
    response = jsonify(user_dto)

    response.status_code = 201
    return response


@app.route('/users/activate/<string:token>')
def activate_user(token: str):
    Auth.activate_user(token)
    return "Congratulation. You are active now! ;)", 200


@app.route('/users/gettoken', methods=['POST'])
@check_request_data(keys=('username', 'password'))
def get_user_token():
    """authentications. token needed for authorized requests"""
    data = request.get_json()

    token = Auth.get_user_token(data['username'], data['password'])

    return jsonify(access_token=token), 200
