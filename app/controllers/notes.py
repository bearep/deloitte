# -*- coding: utf-8 -*-
# Author: bear
from flask import jsonify, url_for, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from app import app
from app.controllers.base import check_request_data
from app.resolvers.note_resolvers import Notes


@app.route('/notes', methods=["POST"])
@jwt_required
# @check_request_data(keys=('title', 'text'))
@check_request_data
def create_note():
    data = request.get_json()

    current_user_id = get_jwt_identity()

    note_dto = Notes.create_note(current_user_id, data)
    response = jsonify(note_dto)
    response.status_code = 201

    response.headers['Location'] = url_for("get_note", note_id=note_dto['id'])
    return response


@app.route('/notes/<int:note_id>')
@jwt_required
def get_note(note_id: str):
    current_user_id = get_jwt_identity()
    note_dto = Notes.get_note(note_id, current_user_id)
    response = jsonify(note_dto)
    response.status_code = 200
    return response


@app.route('/notes')
@jwt_required
def get_notes():
    current_user_id = get_jwt_identity()
    note_dto = Notes.get_notes(current_user_id)
    response = jsonify(note_dto)
    response.status_code = 200
    return response


@app.route('/notes/<int:note_id>', methods=['DELETE'])
@jwt_required
def delete_note(note_id: str):
    current_user_id = get_jwt_identity()
    Notes.delete_note(note_id, current_user_id)
    response = jsonify({})
    response.status_code = 200
    return response


@app.route('/notes/<int:note_id>', methods=['PUT'])
@jwt_required
@check_request_data
def edit_note(note_id: int):
    data = request.get_json()
    current_user_id = get_jwt_identity()
    note_dto = Notes.edit_note(note_id, current_user_id, data)
    response = jsonify(note_dto)
    response.status_code = 200
    response.headers['Location'] = url_for("get_note", note_id=note_dto['id'])
    return response
