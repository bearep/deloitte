# -*- coding: utf-8 -*-
# Author: bear
from functools import partial, wraps
from flask import request

from app.errors.project_errors import UnacceptableRequestException, BadRequestException


def check_request_data(func=None, *, keys=tuple()):
    if func is None:
        return partial(check_request_data, keys=keys)

    @wraps(func)
    def wrapper(*args, **kwargs):
        if not request.accept_mimetypes["application/json"]:
            raise UnacceptableRequestException
        if not request.is_json:
            raise BadRequestException("missing JSON in request")
        data = request.get_json(silent=True)
        if data is None:
            raise BadRequestException("incorrect JSON in request")
        absent_keys = [key for key in keys if key not in data]
        if absent_keys:
            raise BadRequestException(f"Request should contain next fields: {', '.join(absent_keys)}")
        return func(*args, **kwargs)
    return wrapper
