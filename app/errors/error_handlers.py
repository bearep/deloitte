# -*- coding: utf-8 -*-
# Author: bear

from app import app
from app.errors.errors import error_response
from app.errors.project_errors import UnacceptableRequestException, BadRequestException, NotEnoughRightsException, \
    NotFoundException, AccDoesntActivatedException, LoginException, DataValidationException


@app.errorhandler(404)
@app.errorhandler(NotFoundException)
def not_found_error(error):
    return error_response(404, error.description)


@app.errorhandler(401)
@app.errorhandler(LoginException)
def unauthenticated(error):
    return error_response(401, error.description)


@app.errorhandler(500)
def internal_error(error):
    return error_response(500, error.description)


@app.errorhandler(UnacceptableRequestException)
def unacceptable_request(error):
    return error_response(406, error.description)


@app.errorhandler(BadRequestException)
@app.errorhandler(DataValidationException)
def bad_request(error):
    return error_response(400, error.description)


@app.errorhandler(NotEnoughRightsException)
def not_enough_rights(error):
    return error_response(403, error.description)


@app.errorhandler(AccDoesntActivatedException)
def account_doesnt_activated(error):
    return error_response(400, error.description)



