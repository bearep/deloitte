# -*- coding: utf-8 -*-
# Author: bear


class ProjectException(Exception):
    description = None


class UnacceptableRequestException(Exception):
    def __init__(self):
        super().__init__()
        self.description = "server send only JSON"


class BadRequestException(Exception):
    def __init__(self, desc=None):
        super().__init__()
        self.description = desc


class UserNotUniqueException(ProjectException):
    description = "Such Username already present in the syste"


class EmailNotUniqueException(ProjectException):
    description = "Such Email already present in the system"


class NotEnoughRightsException(ProjectException):
    description = "Current user doesn't have rights for this operation"


class NotFoundException(ProjectException):
    description = "Not Found"


class AccDoesntActivatedException(ProjectException):
    description = "Account doesn't activated"


class LoginException(ProjectException):
    description = "Wrong username or password"


class DataValidationException(ProjectException):
    def __init__(self, messages):
        self.description = f"Data validation errors: {messages}"
