# -*- coding: utf-8 -*-
# Author: bear
from marshmallow_sqlalchemy import fields

from app import ma
from app.models.models import User, Note


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
        exclude = ('notes',)
        dump_only = ('id', 'username', 'email', 'activated')
        load_only = ('password_hash',)


class NoteSchema(ma.ModelSchema):
    author = fields.Nested(UserSchema)
    _links = ma.Hyperlinks({"self": ma.URLFor("get_note", note_id="<id>"), "collection": ma.URLFor("get_notes")})

    class Meta:
        model = Note
        include_fk = True
        load_only = ('user_id',)


user_schema = UserSchema()
note_schema = NoteSchema()
