# -*- coding: utf-8 -*-
# Author: bear

from app import db


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(129))
    activated = db.Column(db.BOOLEAN, default=False)
    notes = db.relationship('Note', lazy='dynamic')

    def __repr__(self):
        return f'<User {self.username}>'


class Note(db.Model):
    __tablename__ = "note"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    text = db.Column(db.String(1000), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    # author = db.relationship("User", lazy="joined")

    def __repr__(self):
        return f'<Recipe {self.title}>'

