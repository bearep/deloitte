# -*- coding: utf-8 -*-
# Author: bear

from flask import Flask
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)

jwt = JWTManager(app)

from app.errors import error_handlers
from app.controllers import base, auth, notes
from app.models import models
