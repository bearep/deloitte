# -*- coding: utf-8 -*-
# Author: bear
import re

import jwt
from flask import url_for
from flask_jwt_extended import create_access_token
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, app
from app.models.model_schemas import user_schema
from app.models.models import User
from app.errors.project_errors import BadRequestException, AccDoesntActivatedException, LoginException
from app.workers.send_mail import send_async_email


class Auth:

    @classmethod
    def create_user(cls, username: str, email: str, password: str) -> dict:
        username = username.strip()
        email = email.strip()
        if not cls.is_correct_username(username) or User.query.filter_by(username=username).first():
            raise BadRequestException('please use a different username')
        if not cls.is_correct_email(email) or User.query.filter_by(email=email).first():
            raise BadRequestException('please use a different email address')
        if not cls.is_correct_password(password):
            raise BadRequestException('please use a more complex password')

        password_hash = generate_password_hash(password)
        user = User(username=username, email=email, password_hash=password_hash)
        db.session.add(user)
        db.session.commit()
        return user_schema.dump(user)

    @classmethod
    def activate_user(cls, token: str) -> dict:
        user_id = cls.get_user_id_by_token(token)
        if not user_id:
            raise BadRequestException("wrong token")
        user = User.query.get(user_id)
        if not user:
            raise BadRequestException("wrong token")
        user.activated = True
        db.session.commit()
        return user_schema.dump(user)

    @staticmethod
    def get_user_token(username: str, password: str) -> str:
        """check username and password and return JWT access token
        returned None if username or password is incorrect"""
        user = User.query.filter_by(username=username).first()
        if not user or not check_password_hash(user.password_hash, password):
            raise LoginException
        if user and not user.activated:
            raise AccDoesntActivatedException
        access_token = create_access_token(identity=user.id)
        return access_token

    @staticmethod
    def get_user_id_by_token(token: str) -> int:
        """parse JWT token and return user_id from token payload
        return None if token is incorrect"""
        try:
            user_id = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['activate']
            return user_id
        except:
            return

    @staticmethod
    def get_activation_token(user_id: int):
        """create token for user activation"""
        token = jwt.encode({'activate': user_id}, app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')
        return token

    @staticmethod
    def is_correct_username(username: str) -> bool:
        if username:
            username = username.strip()
            return len(username) >= 1 and username[0].isalpha()

    @staticmethod
    def is_correct_email(email: str) -> bool:
        if email:
            return re.fullmatch("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", email) is not None

    @staticmethod
    def is_correct_password(password: str) -> bool:
        if password:
            return len(password) > 0

    @classmethod
    def send_activation_email(cls, user_id: int, email: str):
        """send email with link for account activation"""
        activation_token = cls.get_activation_token(user_id)
        data = {'subject': "account activation",
                'to': email,
                "body": url_for('activate_user', token=activation_token, _external=True)}
        send_async_email.delay(data)
