# -*- coding: utf-8 -*-
# Author: bear
from typing import List

from marshmallow import ValidationError

from app import db
from app.models.model_schemas import note_schema
from app.models.models import Note
from app.errors.project_errors import NotFoundException, NotEnoughRightsException, DataValidationException


class Notes:
    @staticmethod
    def create_note(user_id: int, data: dict) -> dict:
        data["user_id"] = user_id
        try:
            note = note_schema.load(data)
        except ValidationError as er:
            raise DataValidationException(er.messages)
        db.session.add(note)
        db.session.commit()
        return note_schema.dump(note)

    @staticmethod
    def get_note(note_id: int, user_id: int) -> dict:
        note = Note.query.get(note_id)
        if not note:
            raise NotFoundException
        if note.user_id != user_id:
            raise NotEnoughRightsException
        note_dto = note_schema.dump(note)
        return note_dto

    @staticmethod
    def get_notes(user_id: int) -> List[dict]:
        notes = Note.query.filter_by(user_id=user_id)
        notes_dto = note_schema.dump(notes, many=True)
        return notes_dto

    @staticmethod
    def delete_note(note_id: int, user_id: int):
        note = Note.query.get(note_id)
        if not note:
            raise NotFoundException
        if note.user_id != user_id:
            raise NotEnoughRightsException
        db.session.delete(note)
        db.session.commit()

    @staticmethod
    def edit_note(note_id: int, user_id: int, data: dict) -> dict:
        note = Note.query.get(note_id)
        if not note:
            raise NotFoundException
        if note.user_id != user_id:
            raise NotEnoughRightsException
        try:
            note_schema.load(data, instance=note, partial=('user_id', ))
        except ValidationError as er:
            raise DataValidationException(er.messages)
        db.session.commit()
        note_dto = note_schema.dump(note)
        return note_dto

